from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import rsa, padding


class Verify:
    def __init__(self):
        self.pri_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048,
            backend=default_backend()
        )
        self.pub_key = self.pri_key.public_key()
        self.device_id = self.get_device_id()
        self.message = 'test'

    def get_device_id(self) -> int:
        print("get_device_id method called")
        return 1

    def encrypt_msg(self, msg):
        encrypted = self.pub_key.encrypt(
            msg,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=hashes.SHA256(),
                label=None
            )
        )
        return encrypted

    def verify_data(self, enc_data):
        dec_data = self.pri_key.decrypt(
            enc_data,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=hashes.SHA256(),
                label=None
            )
        )
        if dec_data == self.message:
            return True
        else:
            return False
